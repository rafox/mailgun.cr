# Mailgun.cr

[![build status](https://gitlab.com/rafox/mailgun.cr/badges/master/build.svg)](https://gitlab.com/rafox/mailgun.cr/commits/master)


This is the Mailgun Ruby Library. This library contains methods for easily interacting with the Mailgun API. Below are examples to get you started. 

## Installation

1. Add the dependency to your `shard.yml`:
```yaml
dependencies:
  mailgun.cr:
    gitlab: rafox/mailgun.cr
```
2. Run `shards install`

## Usage

```crystal
require "mailgun.cr"
```

TODO: Write usage instructions here

## Development

TODO: Write development instructions here

## Contributing

1. Fork it (<https://gitlab.com/rafox/mailgun.cr/fork>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [Rafox](https://gitlab.com/rafox) - creator and maintainer
